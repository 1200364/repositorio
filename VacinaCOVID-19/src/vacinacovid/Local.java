/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.Scanner;

/**
 * Um local onde irá decorrer a vacinação.
 * @author João e Dayana
 */
public class Local {

    
    private String nome;
    private String morada;
    private String telefone;

    public Local() {
    }

    /**
     * Cria um novo local, com a informação fornecida.
     * @param nome Nome do local.
     * @param morada Endereço físico do local.
     * @param telefone Contacto telefónico do local.
     */
    public Local(String nome, String morada, String telefone) {
        this.nome = nome;
        this.morada = morada;
        this.telefone = telefone;

    }

    /**
     * Apresenta o nome do local.
     * @return Nome do local.
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Altera o nome do local.
     * @param nome Novo nome do local.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Apresenta o endereço físico do local.
     * @return Morada do local.
     */
    public String getMorada() {
        return this.morada;
    }

    /**
     * Altera o endereço físico do local.
     * @param morada Nova morada a atribuir ao local.
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * Apresenta o contacto telefónico do local.
     * @return Contacto telefónico.
     */
    public String getTelefone() {
        return this.telefone;
    }

    /**
     * Altera o contacto telefónico do local.
     * @param telefone Novo contacto telefónico.
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    @Override
    public String toString() {
        return "\nNome: " + this.nome + "\nMorada: " + this.morada + "\nTelefone: " + this.telefone;
    }

    /**
     * Cria um novo local, de acordo com os dados pedidos ao utilizador.
     * @return Novo local.
     */
    public static Local createLocal() {
        Scanner sc = new Scanner(System.in);

        Local local = new Local();

        System.out.println("--------LOCAL:--------");

        boolean validacao = false;
        String tipoLocal;

        do {
            System.out.println("Tipo de local para a marcação (H, C ou P):");
            tipoLocal = sc.next();

            if (tipoLocal.equalsIgnoreCase("H")) {
                local = Hospital.createHospital();
                validacao = true;
            } else if (tipoLocal.equalsIgnoreCase("P")) {
                local = Pavilhao.createPavilhao();
                validacao = true;
            } else if (tipoLocal.equalsIgnoreCase("C")) {
                validacao = true;
            }
            if (!validacao) {
                System.out.println("Insira um tipo de local válido ! ");
            }

        } while (!validacao);

        if (tipoLocal.equalsIgnoreCase("C")) {
            System.out.println("Insira o nome do Centro de saúde:");
            String nome = sc.next();
            System.out.println("Insira a morada:");
            String morada = sc.next();
            System.out.println("Insira o telefone:");
            String telefone = sc.next();

            local = new Local(nome, morada, telefone);
        }
        return local;
    }
}
