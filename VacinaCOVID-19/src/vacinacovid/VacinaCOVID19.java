/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class VacinaCOVID19 {

    public static List<Marcacao> listaMarcacoes = new ArrayList<>();
    public static String[] arrayDistrito = {"Aveiro", "Beja", "Braga", "Bragança", "Castelo Branco", "Coimbra", "Évora", "Faro", "Guarda", "Leiria", "Lisboa", "Portalegre", "Porto", "Santarém", "Setúbal", "Viana do Castelo", "Vila Real", "Viseu"};


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner opc = new Scanner(System.in);

        int opcao, opcaoPesquisar, opcaoVacina, opcaoAlterar;
        boolean validacao=false;
        String distrito;
        
        do {
                System.out.println("Insira o distrito a gerir:");
                distrito = opc.next();
                
                for (int i = 0; i < 18; i++) {
                    if (distrito.equalsIgnoreCase(arrayDistrito[i])) {
                        validacao = true;
                    }
                }
                if (!validacao) {
                    System.out.println("Insira um distrito válido ! ");
                }

            } while (!validacao);
        
        Marcacao.readMarcacaoFromFile(distrito);

        
        do {
            System.out.println("-------Vacinação Covid-19---------");
            System.out.println("======Menu Principal========\n");
            System.out.println("|     1 - Nova Marcação                      ");
            System.out.println("|     2 - Pesquisar Marcação               ");
            System.out.println("|     3 - Alterar Marcação                    ");
            System.out.println("|     4 - Anular Marcação                    ");
            System.out.println("|     5 - Listagem indivíduos               ");
            System.out.println("|     0 - Sair                                         ");
            System.out.println("=======================\n");
            System.out.print("\n");

            opcao = opc.nextInt();

            switch (opcao) {
                case 0:
                    break;
                case 1:
                    listaMarcacoes.add(Marcacao.createMarcacao());
                    break;
                case 2:
                    do {
                        System.out.println("-------Vacinação Covid-19----------");
                        System.out.println("======Pesquisar por:========\n");
                        System.out.println("|     1 - Utente                                 ");
                        System.out.println("|     2 - Intervalo de idade utentes  ");
                        System.out.println("|     3 - Marca ou lote da vacina      ");
                        System.out.println("|     4 - Local                  ");
                        System.out.println("|     5 - Dia                          ");
                        System.out.println("|     0 - Voltar                                  ");
                        System.out.println("==========================\n");
                        System.out.print("\n");

                        opcaoPesquisar = opc.nextInt();
                        switch (opcaoPesquisar) {
                            case 0:
                                break;
                            case 1:
                                System.out.println("Número do utente: ");
                                String numeroUtente = opc.next();
                                System.out.println(Marcacao.getMarcacaoByUtente(numeroUtente).toString());
                                break;
                            case 2:
                                System.out.println("Indique o intervalo de idades a verificar:");
                                System.out.println("Idade 1:");
                                int idade1 = opc.nextInt();
                                System.out.println("Idade 2:");
                                int idade2 = opc.nextInt();
                                Marcacao.getMarcacaoBetweenAges(idade1, idade2);
                                break;
                            case 3:
                                do {
                                    System.out.println("-------Vacinação Covid-19----------");
                                    System.out.println("======Pesquisar por:========\n");
                                    System.out.println("|     1 - Marca                                 ");
                                    System.out.println("|     2 - Lote                                    ");
                                    System.out.println("|     3 - Marca e lote                                  ");
                                    System.out.println("|     0 - Voltar");
                                    System.out.println("=======================\n");
                                    System.out.print("\n");

                                    opcaoVacina = opc.nextInt();
                                    switch (opcaoVacina) {
                                        case 0:
                                            break;
                                        case 1:
                                            System.out.println("Marca: ");
                                            String marca = opc.next();
                                            Marcacao.getMarcacaoByMarcaVacina(marca).forEach(marcacao -> {System.out.println(marcacao.toString());});
                                            break;

                                        case 2:
                                            System.out.println("Lote: ");
                                            String lote = opc.next();
                                            Marcacao.getMarcacaobyLoteVacina(listaMarcacoes,lote).forEach(marcacao ->{System.out.println(marcacao.toString());});
                                            break;
                                        case 3:
                                            System.out.println("Marca: ");
                                            String marcaVacina = opc.next();
                                            System.out.println("Lote: ");
                                            String loteVacina = opc.next();
                                            Marcacao.getMarcacaoByLoteAndMarca(loteVacina, marcaVacina).forEach(marcacao -> {System.out.println(marcacao.toString());});
                                            break;
                                        default:
                                            System.out.println("Opção Inválida!");
                                            break;
                                    }
                                } while (opcaoVacina != 0);
                                break;
                            case 4:
                                Marcacao.getMarcacaoByLocal();
                                break;
                            case 5:
                                System.out.println("Indique o dia a apresentar(Formato DD-MM-AAAA):");
                                String dia = opc.next();
                                Marcacao.getMarcacaoByDay(dia);
                                break;
                            default:
                                System.out.println("Opção Inválida!");
                                break;
                        }
                    } while (opcaoPesquisar != 0);
                    break;
                case 3:
                    do {
                        System.out.println("-------Vacinação Covid-19----------");
                        System.out.println("======Alterar Marcações========\n");
                        System.out.println("|     1 - Local            ");
                        System.out.println("|     2 - Data             ");
                        System.out.println("|     3 - Hora             ");
                        System.out.println("|     4 - Enfermeiro   ");
                        System.out.println("|     0 - Voltar           \n");
                        System.out.println("==========================\n");
                        System.out.print("\n");

                        opcaoAlterar = opc.nextInt();
                        switch (opcaoAlterar) {
                            case 0:
                                break;
                            case 1:
                                System.out.println("Número Utente:");
                                Marcacao.alteraLocalMarcacao(opc.next());
                                break;
                            case 2:
                                System.out.println("Número de utente:");
                                String numUtente = opc.next();
                                System.out.println("Nova Data:");
                                String data = opc.next();
                                Marcacao.alteraDataMarcacao(numUtente, data);
                                break;
                            case 3:
                                System.out.println("Número de utente:");
                                String numeroUtente = opc.next();
                                System.out.println("Nova Data:");
                                String hora = opc.next();
                                Marcacao.alteraHoraMarcacao(numeroUtente,hora);
                                break;
                            case 4:
                                System.out.println("Número de Utente:");
                                Marcacao.alteraEnfermeiroMarcacao(opc.next());
                                break;
                            default:
                                System.out.println("Opção Inválida!");
                                break;
                        }
                    } while (opcaoAlterar != 0);
                    break;
                case 4:
                    System.out.println("Número do utente: ");
                    String numeroUtente = opc.nextLine();
                    Marcacao.removeMarcacao(numeroUtente);
                    break;
                case 5:
                    System.out.println("Insira uma data(Formato DD-MM-AAAA):");
                    String data = opc.next();
                    Local local = Local.createLocal();
                    Pessoa.getPeopleInLocalAndDay(local, data);
                    break;
                default:
                    System.out.println("Opção Inválida!");
                    break;
            }
        } while (opcao != 0);

        Marcacao.writeMarcacaoToFile(distrito);
    }

}
