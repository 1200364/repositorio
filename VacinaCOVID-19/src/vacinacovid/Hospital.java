
package vacinacovid;

import java.util.Scanner;

/**
 * Hospital onde será realizada a vacinação. Hospital é um Local.
 * @author João e Dayana
 */
public class Hospital extends Local {

    private String nomeEdificio;
    private String extensaoTel;

    public Hospital() {
    }

    /**
     * Cria um novo hospital, com toda a informação introduzida.
     * @param nome Nome oficial do hospital
     * @param morada Endereço físico do hospital
     * @param telefone Contacto oficial do hospital
     * @param nomeEdificio Nome do edíficio específico onde irá decorrer a vacinação.
     * @param extensaoTel Extensão telefónica do hospital dentro do SNS.
     */
    public Hospital(String nome, String morada, String telefone, String nomeEdificio, String extensaoTel) {

        super(nome,morada,telefone);
        this.nomeEdificio = nomeEdificio;
        this.extensaoTel = extensaoTel;
    }

    /**
     * Apresenta o nome do edíficio específico onde irá decorrer a vacinação.
     * @return Nome do edifício.
     */
    public String getNomeEdificio() {
        return this.nomeEdificio;
    }

    /**
     * Modifica o nome do edifício onde a vacinação irá decorrer.
     * @param nomeEdificio Novo nome do edíficio específico onde irá decorrer a vacinação.
     */
    public void setNomeEdificio(String nomeEdificio) {
        this.nomeEdificio = nomeEdificio;
    }

    /**
     * Apresenta a extensão telefónica do hospital dentro do SNS.
     * @return Extensão telefónica do hospital.
     */
    public String getExtensaoTel() {
        return this.extensaoTel;
    }

    /**
     * Atualiza a extensão telefónica de um hospital dentro do SNS.
     * @param ExtensaoTel Nova extensão telefónica do hospital.
     */
    public void setExtensaoTel(String ExtensaoTel) {
        this.extensaoTel = nomeEdificio;
    }

    @Override
    public String toString() {
        return "\nNome: " + this.getNome() + "\nMorada: " + this.getMorada() + "\nTelefone: " + this.getTelefone() + "\nNome do Edifício: " + this.nomeEdificio + "\nExtensão Telefone: " + this.extensaoTel;
    }

    /**
     * Pede dados de um novo hospital a ser criado, ao utilizador.
     * @return Novo hospital.
     */
    public static Hospital createHospital() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nome do hospital:");
        String nome = sc.next();
        System.out.println("Morada:");
        String morada = sc.next();
        System.out.println("Telefone:");
        String telefone = sc.next();
        System.out.println("Edifício/bloco:");
        String nomeEdificio = sc.next();
        System.out.println("Extensão Telefónica:");
        String extensaoTel = sc.next();

        return new Hospital(nome, morada, telefone, nomeEdificio, extensaoTel);
    }
}
