/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.Scanner;

/**
 * A vacina prevista a ser administrada para uma marcação.
 * @author João e Dayana.
 */
public class Vacina {
    private String marca;
    private String lote;
    
    public Vacina(){}
    
    /**
     * Cria a vacina a ser associada à marcação.
     * @param marca Marca da vacina.
     * @param lote Lote da vacina.
     */
    public Vacina(String marca, String lote){
        this.marca = marca;
        this.lote = lote;
    }
    
    /**
     * Apresenta a marca da vacina.
     * @return Marca da vacina.
     */
    public String getMarca(){
        return this.marca;
    }

    /**
     * Altera a marca da vacina a ser administrada.
     * @param marca Nova marca.
     */
    public void setMarca(String marca){
        this.marca = marca;
    }

    /**
     * Apresenta o lote da vacina a ser administrada.
     * @return Lote da vacina.
     */
    public String getLote(){
        return this.lote;
    }

    /**
     * Altera o lote da vacina a ser administrada.
     * @param lote Novo lote.
     */
    public void setLote(String lote){
        this.lote = lote;
    }
    @Override
    public String toString(){
        return "\nMarca: " + this.marca + "\nLote: " + this.lote;
    }
    
    /**
     * Cria uma nova vacina, com base em informação introduzida pelo utilizador.
     * @return Nova vacina.
     */
    public static Vacina createVacina(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("-----------Vacina:----------");
        System.out.println("Marca:");
        String marca = sc.next();
        System.out.println("Lote:");
        String lote = sc.next();
        
        return new Vacina(marca,lote);
    }
}
