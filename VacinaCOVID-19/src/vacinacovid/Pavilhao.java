/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.Scanner;

/**
 * Pavilhão onde irá decorrer a vacinação. Pavilhão é um local.
 * @author João e Dayana
 */
public class Pavilhao extends Local{

    private int numeroSeccao;
    
    public Pavilhao(){}
    
    /**
     * Cria um novo pavilhão de acordo com os parâmetros dados.
     * @param nome Nome do pavilhão.
     * @param morada Endereço físico do pavilhão.
     * @param telefone Número telefónico do pavilhão.
     * @param numeroSeccao Número de secção onde irá decorrer a vacinação.
     */
    public Pavilhao(String nome, String morada, String telefone, int numeroSeccao){
        
        super(nome, morada, telefone);
        this.numeroSeccao = numeroSeccao;
        
    }
    
    /**
     * Obter o número da secção onde irá decorrer a vacinação.
     * @return Número da secção.
     */
    public int getNumeroSeccao(){
        return this.numeroSeccao;
    }
    
    /**
     * Altera o número da secção onde irá decorrer a vacinação.
     * @param numeroSeccao Novo número de secção.
     */
    public void setNumeroseccao(int numeroSeccao){
        this.numeroSeccao = numeroSeccao;
    }
    
   @Override
   public String toString(){
       return "\nNome: " + this.getNome() + "\nMorada: " + this.getMorada() + "\nTelefone: " + this.getTelefone()+ "\nNúmero da Secção: " +this.numeroSeccao;
   }  
    
    /**
     * Cria um novo pavilhão, pedindo os dados ao utilizador.
     * @return Novo pavilhão.
     */
    public static Pavilhao createPavilhao(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Insira o nome do pavilhão:");
        String nome = sc.next();
        System.out.println("Insira a morada:");
        String morada = sc.next();
        boolean validacao = false;
        String distrito;
        do {
            System.out.println("Distrito:");
            distrito = sc.next();

            for (int i = 0; i < 18; i++) {
                if (distrito.equalsIgnoreCase(VacinaCOVID19.arrayDistrito[i])) {
                    validacao = true;
                    break;
                }
            }
            if (!validacao) {
                System.out.println("Insira um distrito válido ! ");
            }

        } while (!validacao);
        System.out.println("Insira o telefone:");
        String telefone = sc.next();
        System.out.println("Número da secção:");
        int numSeccao = sc.nextInt();
        
        return new Pavilhao(nome,morada,telefone,numSeccao);
   }
}
