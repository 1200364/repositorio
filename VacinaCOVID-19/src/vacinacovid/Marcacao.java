package vacinacovid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Marcação para uma vacinação contra o vírus covid-19.
 *
 * @author João e Dayana
 */
public class Marcacao {

    private Local local;
    private String data;
    private String hora;
    private Enfermeiro enfermeiro;
    private Utente utente;
    private Vacina vacina;

    public Marcacao() {
    }

    /**
     * Cria uma nova marcação, com a informação fornecida.
     *
     * @param local Um local onde irá ser realizada a vacinação.
     * @param data Uma data para a marcação ser cumprida.
     * @param hora Uma hora para a marcação ser cumprida.
     * @param enfermeiro Um enfermeiro que irá realizar a vacinação.
     * @param utente Um utente que irá receber a vacina.
     * @param vacina Uma dose da vacina que irá ser aplicada no utente.
     */
    public Marcacao(Local local, String data, String hora, Enfermeiro enfermeiro, Utente utente, Vacina vacina) {
        this.local = local;
        this.data = data;
        this.hora = hora;
        this.enfermeiro = enfermeiro;
        this.utente = utente;
        this.vacina = vacina;
    }

    /**
     * Apresenta o local onde irá ser realizada a vacinação
     *
     * @return Local relativo à marcação.
     */
    public Local getLocal() {
        return this.local;
    }

    /**
     * Altera o local relativo à marcação.
     *
     * @param local Novo local.
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * Apresenta a data em que a vacinação será realizada.
     *
     * @return Data da vacinação
     */
    public String getData() {
        return this.data;
    }

    /**
     * Define nova data para a realização da vacinação.
     *
     * @param data Nova data para a marcação.
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Apresenta a hora em que a vacinação irá decorrer.
     *
     * @return Hora da vacinação.
     */
    public String getHora() {
        return this.hora;
    }

    /**
     * Define uma nova hora para a marcação ser realizada.
     *
     * @param hora Nova hora para a vacinação.
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * Apresenta a informação do enfermeiro que irá realizar a vacinação.
     *
     * @return Enfermeiro associado à marcação.
     */
    public Enfermeiro getEnfemeiro() {
        return this.enfermeiro;
    }

    /**
     * Altera o enfermeiro que irá realizar a vacinação.
     *
     * @param enfermeiro Novo enfermeiro.
     */
    public void setEnfermeiro(Enfermeiro enfermeiro) {
        this.enfermeiro = enfermeiro;
    }

    /**
     * Apresenta a informação do utente que irá receber a vacina.
     *
     * @return Utente associado à marcação.
     */
    public Utente getUtente() {
        return this.utente;
    }

    /**
     * Altera o utente associado à marcação.
     *
     * @param utente Novo utente.
     */
    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    /**
     * Informação da vacina a ser administrada nesta marcação.
     *
     * @return Vacina a ser administrada.
     */
    public Vacina getVacina() {
        return this.vacina;
    }

    /**
     * Altera a vacina a ser administrada na marcação.
     *
     * @param vacina Nova vacina.
     */
    public void setVacina(Vacina vacina) {
        this.vacina = vacina;
    }

    @Override
    public String toString() {
        return "\n---LOCAL: ---" + this.local.toString() + "\nData: " + this.data + "\nHora: " + this.hora + "\n---ENFERMEIRO: ---" + this.enfermeiro.toString() + "\n---UTENTE: ---" + this.utente.toString() + "\n---VACINA: ---" + this.vacina.toString();
    }

    /**
     * Pede os dados ao utilizador para que seja possível criar uma nova
     * marcação.
     *
     * @return Nova marcação.
     */
    public static Marcacao createMarcacao() {
        Scanner scan = new Scanner(System.in);
        Local local = Local.createLocal();
        System.out.println("Insira a data da marcação (DD-MM-AAAA):");
        String data = scan.next();
        System.out.println("Insira a hora da marcação:");
        String hora = scan.next();
        Enfermeiro enfermeiro = Enfermeiro.createEnfermeiro();
        Utente utente = Utente.createUtente();
        Vacina vacina = Vacina.createVacina();

        return new Marcacao(local, data, hora, enfermeiro, utente, vacina);
    }

    /**
     * Percorre a lista de todas as marcações e devolve a marcação associada ao
     * número de utente referido.
     *
     * @param numeroUtente
     * @return Marcação associado ao número de utente.
     */
    public static Marcacao getMarcacaoByUtente(String numeroUtente) {
        Marcacao marcacaoDoUtente = null;

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                marcacaoDoUtente = marcacao;
                break;
            }

        }
        return marcacaoDoUtente;
    }

    /**
     * Remove uma marcação da lista de todas as marcações.
     *
     * @param numUtente Número de utente associado à marcação a ser removida.
     */
    public static void removeMarcacao(String numUtente) {
        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numeroUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                VacinaCOVID19.listaMarcacoes.remove(marcacao);
                break;
            }

        }
    }

    /**
     * Percorre a lista de todas as marcações e devolve uma lista com todas as
     * marcações associadas à marca da vacina referida.
     *
     * @param marca Marca a ser comparada.
     * @return Lista com todas as marcações compatíveis com a marca pedida.
     */
    public static List<Marcacao> getMarcacaoByMarcaVacina(String marca) {
        List<Marcacao> marcacoesByMarca = new ArrayList<>();

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {

            String marcaVacina = marcacao.getVacina().getMarca();

            if (marcaVacina.equals(marca)) {
                marcacoesByMarca.add(marcacao);
                break;
            }

        }
        return marcacoesByMarca;
    }

    /**
     * Percorre uma lista de marcações e devolve uma lista das marcações associadas a um lote de vacina referido.
     * @param lote Lote de vacinas a ser testado.
     * @return Lista de todas as vacinas correspondentes.
     */
    public static List<Marcacao> getMarcacaobyLoteVacina(List<Marcacao> lista,String lote) {
        List<Marcacao> marcacoesByLote = new ArrayList<>();

        for (Marcacao marcacao : lista) {

            String loteVacina = marcacao.getVacina().getLote();

            if (loteVacina.equalsIgnoreCase(lote)) {
                marcacoesByLote.add(marcacao);
                break;
            }

        }
        return marcacoesByLote;
    }
    
    /**
     * Cria uma lista de marcações filtradas por um dado lote e marca.
     * @param lote Lote a ser filtrado.
     * @param marca Marca a ser filtrada.
     * @return
     */
    public static List<Marcacao> getMarcacaoByLoteAndMarca(String lote, String marca){
        return getMarcacaobyLoteVacina(getMarcacaoByMarcaVacina(marca),lote); 
    }

    /**
     * Altera o local de uma marcação.
     * @param numeroUtente Número do utente à qual a marcação a ser alterada está associado.
     */
    public static void alteraLocalMarcacao(String numeroUtente) {
        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                marcacao.setLocal(Local.createLocal());
                break;
            }

        }
    }

    /**
     * Altera a data de uma marcação.
     * @param numeroUtente Número do utente à qual a marcação a ser alterada está associado.
     * @param data Nova data a ser associada à marcação.
     */
    public static void alteraDataMarcacao(String numeroUtente, String data) {

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                marcacao.setData(data);
                break;
            }

        }

    }

    /**
     * Altera a hora de uma marcação.
     * @param numeroUtente Número do utente à qual a marcação a ser alterada está associado.
     * @param hora Nova hora para a marcação.
     */
    public static void alteraHoraMarcacao(String numeroUtente, String hora) {

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                marcacao.setHora(hora);
                break;
            }

        }

    }

    /**
     * Altera o enfermeiro de uma marcação.
     * @param numeroUtente Número do utente à qual a marcação a ser alterada está associado.
     */
    public static void alteraEnfermeiroMarcacao(String numeroUtente) {

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            String numUtente = marcacao.getUtente().getNumeroUtente();

            if (numUtente.equals(numeroUtente)) {
                marcacao.setEnfermeiro(Enfermeiro.createEnfermeiro());
                break;
            }

        }

    }

    /**
     * Obter todas as marcações de utentes entre o respectivo intervalo de idades.
     * @param idade1 1º valor do intervalo de idades.
     * @param idade2 2º valor do intervalo de idades.
     */
    public static void getMarcacaoBetweenAges(int idade1, int idade2) {
        int idadeMaior, idadeMenor;

        for (Marcacao marcacao : VacinaCOVID19.listaMarcacoes) {
            Utente utente = marcacao.getUtente();
            String dataNasc = utente.getDataNascimento();
            int anoNasc = Integer.parseInt(dataNasc.substring(dataNasc.length() - 4));
            int anoAtual = Year.now().getValue();
            int idade = anoAtual - anoNasc;

            if (idade1 < idade2) {
                idadeMaior = idade2;
                idadeMenor = idade1;
            } else {
                idadeMaior = idade1;
                idadeMenor = idade2;
            }

            if (idade <= idadeMaior && idade >= idadeMenor) {
                System.out.println(marcacao.toString());
            }
        }
    }

    /**
     * Obter todas as marcações em um determinado dia.
     * @param data Data a ser filtrada.
     */
    public static void getMarcacaoByDay(String data) {

        VacinaCOVID19.listaMarcacoes.forEach(marcacao -> {
            String dataMarcacao = marcacao.getData();
            if (dataMarcacao.equals(data)) {
                System.out.println(marcacao.toString());
            }
        });
    }

    /**
     * Lista toas as marcações para um local de vacinação indicado pelo utilizador
     */
    public static void getMarcacaoByLocal() {
        System.out.println("Insira o nome do Centro de saúde:");
        Scanner opc = new Scanner(System.in);
        String nomeCentro = opc.nextLine();

        VacinaCOVID19.listaMarcacoes.forEach(marcacao -> {
            String nomCentro = marcacao.getLocal().getNome();
            if (nomCentro.equalsIgnoreCase(nomeCentro)) {
                System.out.println(marcacao.toString());
            }
        });
    }

    /**
     * Escreve todas as marcações de um determinado distrito para o respectivo ficheiro.
     * @param distrito Distrito a ser escrito.
     */
    public static void writeMarcacaoToFile(String distrito) {
        File ficheiro = new File("src\\vacinacovid\\Marcacao\\" + distrito + ".txt");

        if (ficheiro.exists()) {
            ficheiro.delete();
        }

        VacinaCOVID19.listaMarcacoes.forEach(marcacao -> {
            try {
                BufferedWriter bfw = new BufferedWriter(new FileWriter(ficheiro, true));
                bfw.write("\n ");

                if (marcacao.getLocal() instanceof Hospital) {
                    bfw.write("\nTipoLocal: H\n");
                } else if (marcacao.getLocal() instanceof Pavilhao) {
                    bfw.write("\nTipoLocal: P\n");
                } else {
                    bfw.write("\nTipoLocal: C\n");
                }

                bfw.write(marcacao.toString().trim());
                bfw.close();

            } catch (Exception ex) {
                Logger.getLogger(Marcacao.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    /**
     * Lê e cria todas as marcações num determinado ficheiro.
     * @param distrito Distrito a ser carregado.
     * 
     */
    public static void readMarcacaoFromFile(String distrito) {
        File ficheiro = new File("src\\vacinacovid\\Marcacao\\" + distrito + ".txt");

        if (ficheiro.exists()) {
            try {

                List<String> infoMarcacao = new ArrayList<>();
                String linha;
                BufferedReader bfr = new BufferedReader(new FileReader(ficheiro));
                String[] coluna;

                do {
                    infoMarcacao.clear();

                    while ((linha = bfr.readLine()) != null && !linha.equalsIgnoreCase(" ")) {
                        coluna = linha.split(": ");
                        infoMarcacao.add(coluna[1]);
                    }

                    Local localMarcacao;
                    String data;
                    String hora;
                    Enfermeiro enfermeiro;
                    Utente utente;
                    Vacina vacina;

                    if (!infoMarcacao.isEmpty()) {

                        if (infoMarcacao.get(0).contains("H")) {
                            localMarcacao = new Hospital(infoMarcacao.get(2), infoMarcacao.get(3), infoMarcacao.get(4), infoMarcacao.get(5), infoMarcacao.get(6));
                            data = infoMarcacao.get(7);
                            hora = infoMarcacao.get(8);
                            enfermeiro = new Enfermeiro(infoMarcacao.get(10), infoMarcacao.get(11), infoMarcacao.get(12));
                            utente = new Utente(infoMarcacao.get(14), infoMarcacao.get(15), infoMarcacao.get(16), infoMarcacao.get(17), infoMarcacao.get(18));
                            vacina = new Vacina(infoMarcacao.get(20), infoMarcacao.get(21));

                            VacinaCOVID19.listaMarcacoes.add(new Marcacao(localMarcacao, data, hora, enfermeiro, utente, vacina));

                        } else if (infoMarcacao.get(0).contains("P")) {
                            localMarcacao = new Pavilhao(infoMarcacao.get(2), infoMarcacao.get(3), infoMarcacao.get(4), Integer.parseInt(infoMarcacao.get(5)));
                            data = infoMarcacao.get(6);
                            hora = infoMarcacao.get(7);
                            enfermeiro = new Enfermeiro(infoMarcacao.get(9), infoMarcacao.get(10), infoMarcacao.get(11));
                            utente = new Utente(infoMarcacao.get(13), infoMarcacao.get(14), infoMarcacao.get(15), infoMarcacao.get(16), infoMarcacao.get(17));
                            vacina = new Vacina(infoMarcacao.get(20), infoMarcacao.get(21));

                            VacinaCOVID19.listaMarcacoes.add(new Marcacao(localMarcacao, data, hora, enfermeiro, utente, vacina));

                        } else if (infoMarcacao.get(0).contains("C")) {
                            localMarcacao = new Local(infoMarcacao.get(2), infoMarcacao.get(3), infoMarcacao.get(4));
                            data = infoMarcacao.get(5);
                            hora = infoMarcacao.get(6);
                            enfermeiro = new Enfermeiro(infoMarcacao.get(8), infoMarcacao.get(9), infoMarcacao.get(10));
                            utente = new Utente(infoMarcacao.get(12), infoMarcacao.get(13), infoMarcacao.get(14), infoMarcacao.get(15), infoMarcacao.get(16));
                            vacina = new Vacina(infoMarcacao.get(18), infoMarcacao.get(19));

                            VacinaCOVID19.listaMarcacoes.add(new Marcacao(localMarcacao, data, hora, enfermeiro, utente, vacina));
                        }
                    }

                } while (linha != null);
                bfr.close();
            } catch (Exception ex) {
                System.out.println("Erro ao ler do ficheiro :" + ex.getMessage());
            }

        }
    }
}
