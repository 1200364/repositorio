/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa uma pessoa.
 * @author João e Dayana
 */
public class Pessoa {

    private String nome;
    private String numeroTelemovel;

    public Pessoa() {
    }

    /**
     * Cria uma nova pessoa, com as informações obtidas.
     * @param nome Nome da Pessoa.
     * @param numeroTelemovel Contacto telefónico da pessoa.
     */
    public Pessoa(String nome, String numeroTelemovel) {
        this.nome = nome;
        this.numeroTelemovel = numeroTelemovel;
    }

    /**
     * Apresenta o nome da pessoa.
     * @return Nome da pessoa.
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Altera o nome da pessoa.
     * @param nome Nome a alterar.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Apresenta o contacto telefónico da pessoa.
     * @return Número de telemóvel.
     */
    public String getNumeroTelemovel() {
        return this.numeroTelemovel;
    }

    /**
     * Altera o contacto telefónico da pessoa.
     * @param numeroTelemovel Novo número de telemóvel.
     */
    public void setNumeroTelemovel(String numeroTelemovel) {
        this.numeroTelemovel = numeroTelemovel;
    }

    @Override
    public String toString() {
        return "\nNome: " + this.nome + "\nNúmero de Telemóvel: " + this.numeroTelemovel;
    }

    /**
     * Lista todas as pessoas presentes num determinado local e data.
     * @param local Local a filtrar.
     * @param data Data a filtrar.
     */
    public static void getPeopleInLocalAndDay(Local local, String data) {
        List<Pessoa> pessoasPresentes = new ArrayList<>();

        VacinaCOVID19.listaMarcacoes.forEach(marcacao -> {
            if (local.equals(marcacao.getLocal()) && data.equalsIgnoreCase(marcacao.getData())) {

                Enfermeiro enfermeiro = marcacao.getEnfemeiro();
                if (!pessoasPresentes.contains(enfermeiro)) {
                    pessoasPresentes.add(enfermeiro);
                }

                pessoasPresentes.add(marcacao.getUtente());
            }
        });

        pessoasPresentes.forEach(pessoa -> {
            System.out.println(pessoa.toString());
            if (pessoa instanceof Enfermeiro) {
                System.out.println("Categoria : Enfermeiro");
            } else {
                System.out.println("Categoria : Utente");
            }
        });
    }
}
