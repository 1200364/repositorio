/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.Scanner;

/**
 * Um utente a ser vacinado. Utente é uma pessoa.
 * @author João e Dayana.
 */
public class Utente extends Pessoa{
    
    private String sexo;
    private String dataNascimento;
    private String numeroUtente;
    
    public Utente(){}
    
    /**
     * Cria um novo utente, com os parâmetros determinados.
     * @param nome Nome do utente.
     * @param numeroTel Contacto telefónico do utente.
     * @param sexo Gênero do utente.
     * @param dataNascimento Data de nascimento do utente.
     * @param numeroUtente Número de utente no SNS.
     */
    public Utente(String nome, String numeroTel, String sexo, String dataNascimento, String numeroUtente){
    
        super(nome,numeroTel);
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.numeroUtente = numeroUtente;
        
    }
    
    /**
     * Apresenta o gênero da pessoa.
     * @return gênero da pessoa.
     */
    public String getSexo(){
        return this.sexo;
    }

    /**
     * Modifica o gênero da pessoa.
     * @param sexo Novo gênero da pessoa.
     */
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    
    /**
     * Apresenta a data de nascimento associado à pessoa.
     * @return Data de nascimento.
     */
    public String getDataNascimento(){
        return this.dataNascimento;
    }
    
    /**
     * Altera a data de nascimento da pessoa.
     * @param dataNascimento Nova data de nascimento.
     */
    public void setDataNascimento(String dataNascimento){
        this.dataNascimento = dataNascimento;
    }
    
    /**
     * Apressenta o número de utente no SNS da pessoa.
     * @return Número de utente.
     */
    public String getNumeroUtente(){
        return this.numeroUtente;
    }
    
    /**
     * Altera o número de utente da pessoa.
     * @param numeroUtente Novo número de utente.
     */
    public void setNumeroUtente(String numeroUtente){
        this.numeroUtente = numeroUtente;
    }
    
    @Override
    public String toString(){
        return super.toString() + "\nSexo: " + this.sexo + "\nData de Nascimento: " + this.dataNascimento + "\nNúmero de Utente: " + this.numeroUtente;
    }
    
    /**
     * Cria um novo utente, baseado em dados inseridos pelo utilizador.
     * @return Novo utente.
     */
    public static Utente createUtente(){
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println("-------------Utente:--------------");
        System.out.println("Insira o nome do utente:");
        String nome = sc.next();
        System.out.println("Sexo:");
        String sexo = sc.next();
        System.out.println("Data de nascimento(DD/MM/AAAA): ");
        String dataNascimento = sc.next();
        System.out.println("Número de Utente:");
        String numeroUtente = sc.next();
        System.out.println("Número de telemóvel:");
        String numeroTelemovel = sc.next();
        
        return new Utente(nome,numeroTelemovel,sexo,dataNascimento,numeroUtente);
    }
}
