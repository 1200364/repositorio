/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacinacovid;

import java.util.Scanner;

/**
 *Um enfermeiro que irá realizar a vacinação. Enfermeiro é uma subclasse de Pessoa.
 * 
 * @author João e Dayana
 */
public class Enfermeiro extends Pessoa {
    private String numeroCedulaProfissional;
    
    public Enfermeiro(){}
    
    /**
     * Cria um novo enfermeiro, com os parâmetros pedidos.
     * 
     * @param nome Nome do enfermeiro
     * @param numeroTelemovel Contacto telefónico do enfermeiro
     * @param numeroCedulaProfissional Credencial profissional do enfermeiro
     */
    public Enfermeiro(String nome, String numeroTelemovel, String numeroCedulaProfissional){
        super(nome,numeroTelemovel);
        this.numeroCedulaProfissional = numeroCedulaProfissional;
    }
    
    /**
     *Apresenta a credencial profissional de um enfermeiro.
     * 
     * @return Número de cédula profissional do enfermeiro.
     */
    public String getNumeroCedulaProfissional(){
        return this.numeroCedulaProfissional;
    }

    /**
     *Altera a credencial profissional do enfermeiro
     * @param numeroCedulaProfissional
     */
    public void setNumeroCedulaProfissional(String numeroCedulaProfissional){
        this.numeroCedulaProfissional = numeroCedulaProfissional;
    }
    
    @Override
    public String toString(){
        return super.toString()+"\nNúmero de Cédula Profissional: " + this.numeroCedulaProfissional;
    }
    
    /**
     * Pede os dados de um enfermeiro a ser criado, ao utilizador.
     * @return Novo enfermeiro.
     */
    public static Enfermeiro createEnfermeiro(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("-----------ENFERMEIRO:------------");
        
        System.out.println("Insira o nome do enfermeiro:");
        String nome = sc.next();
        System.out.println("Insira o número da cédula profissional:");
        String numCedula = sc.next();
        System.out.println("Insira o número do telemóvel do enfermeiro:");
        String telemovel = sc.next();
        
        return new Enfermeiro(nome,telemovel,numCedula);
    }
}
